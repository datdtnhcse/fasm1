﻿// Assignment 1

using System;

public class NguoiLaoDong
{
    // attributes
    public string HoTen = "";
    public int NamSinh = 0;
    public int LuongCoBan = 0;

    // methods
    public NguoiLaoDong() { }   
    public NguoiLaoDong(string hoTen, int namSinh, int luongCoBan)
    {
        try
        {
            HoTen = hoTen.Trim();

            if (typeof(int) == namSinh.GetType())
            {
                if (namSinh > 0)
                {
                    NamSinh = namSinh;
                }
                else
                {
                    throw new ArgumentException("Nam sinh phai lon hon 0");
                }
            }
            else
            {
                throw new ArgumentException("Kieu du lieu cua Nam sinh khong hop le");
            }

            if (typeof(int) == luongCoBan.GetType())
            {
                if (luongCoBan > 0)
                {
                    LuongCoBan = luongCoBan;
                }
                else
                {
                    throw new ArgumentException("Luong phai lon hon 0");
                }
            }
            else
            {
                throw new ArgumentException("Kieu du lieu cua luong khong hop le");
            }
        }
        catch (Exception exc)
        {
            Console.WriteLine("Da xay ra loi: " + exc.Message);
            Environment.Exit(0);
        }
    }
    public void NhapThongTin(string hoTen, int namSinh, int luongCoBan)
    {
        try
        {
            HoTen = hoTen.Trim();

            if (typeof(int) == namSinh.GetType())
            {
                if (namSinh > 0)
                {
                    NamSinh = namSinh;
                }
                else
                {
                    throw new ArgumentException("Nam sinh phai lon hon 0");
                }
            }
            else
            {
                throw new ArgumentException("Kieu du lieu cua nam sinh khong hop le");
            }

            if (typeof(int) == luongCoBan.GetType())
            {
                if (luongCoBan > 0)
                {
                    LuongCoBan = luongCoBan;
                }
                else
                {
                    throw new ArgumentException("Luong phai lon hon 0");
                }
            }
            else
            {
                throw new ArgumentException("Kieu du lieu cua luong khong hop le");
            }
        }
        catch (Exception exc)
        {
            Console.WriteLine("Da xay ra loi: " + exc.Message);
            Environment.Exit(0);
        }
    }
    public int TinhLuong()
    {
        return LuongCoBan;
    }
    public virtual void XuatThongTin()
    {
        Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
    }
}

public class GiaoVien : NguoiLaoDong
{
    // attributes
    public decimal HeSoLuong;
    // methods
    public GiaoVien() { }
    public GiaoVien(string hoTen, int namSinh, int luongCoBan, decimal heSoLuong)
                    : base (hoTen, namSinh, luongCoBan)
    {
        try 
        { 
            if (typeof(decimal) == heSoLuong.GetType())
            {
                if (heSoLuong > 0)
                {
                    HeSoLuong = heSoLuong;
                }
                else
                {
                    throw new ArgumentException("He so luong phai lon hon 0");
                }
            }
            else
            {
                throw new ArgumentException("Kieu du lieu cua he so luong khong hop le");
            }

        }
        catch (Exception exc)
        {
            Console.WriteLine("Da xay ra loi: " + exc.Message);
            Environment.Exit(0);
        }
    }
    public void NhapThongTin(decimal heSoLuong)
    {
        try
        {
            if (typeof(decimal) == heSoLuong.GetType())
            {
                if (heSoLuong > 0)
                {
                    HeSoLuong = heSoLuong;
                }
                else
                {
                    throw new ArgumentException("He so luong phai lon hon 0");
                }
            }
            else
            {
                throw new ArgumentException("Kieu du lieu cua He so luong khong hop le");
            }

        }
        catch (Exception exc)
        {
            Console.WriteLine("Da xay ra loi: " + exc.Message);
            Environment.Exit(0);
        }
    }
    public decimal TinhLuong()
    {
        return LuongCoBan * HeSoLuong * 1.25m;
    }
    public override void XuatThongTin()
    {
        Console.WriteLine($"Ho ten: {HoTen}");
        Console.WriteLine($"Nam sinh: {NamSinh}");
        Console.WriteLine($"Luong co ban: {LuongCoBan}");
        Console.WriteLine($"He so luong: {HeSoLuong}");
        Console.WriteLine($"Luong: {TinhLuong()}");
    }
    public decimal XuLy()
    {
        return HeSoLuong + 0.6m;
    }
}

class Program
{
    static void Main(string[] args)
    {
        Console.Write("Nhap so luong giao vien: ");
        int soLuongGiaoVien = Convert.ToInt32(Console.ReadLine());

        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 0; i < soLuongGiaoVien; i++)
        {
            Console.WriteLine($"\nNhap thong tin giao vien thu {i + 1}:");

            Console.Write("Ho ten: ");
            string hoTen = Console.ReadLine().Trim();
            Console.Write("Nam sinh: ");
            int namSinh = Convert.ToInt32(Console.ReadLine());
            Console.Write("Luong co ban: ");
            int luongCoBan = Convert.ToInt32(Console.ReadLine());
            Console.Write("He so luong: ");
            decimal heSoLuong = Convert.ToDecimal(Console.ReadLine());

            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);

            danhSachGiaoVien.Add(giaoVien);
        }

        decimal luongThapNhat = danhSachGiaoVien[0].TinhLuong();
        int viTriLuongThapNhat = 0;

        for (int i = 1; i < danhSachGiaoVien.Count; i++)
        {
            decimal luong = danhSachGiaoVien[i].TinhLuong();

            if (luong < luongThapNhat)
            {
                luongThapNhat = luong;
                viTriLuongThapNhat = i;
            }
        }

        Console.WriteLine("\nThong tin giao vien co luong thap nhat:");
        danhSachGiaoVien[viTriLuongThapNhat].XuatThongTin();

        Console.ReadKey();
    }

}